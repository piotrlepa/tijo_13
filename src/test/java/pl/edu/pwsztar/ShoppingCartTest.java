package pl.edu.pwsztar;

import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static pl.edu.pwsztar.ShoppingCartOperation.PRODUCTS_LIMIT;

public class ShoppingCartTest {

  static ShoppingCartOperation shoppingCart;

  @BeforeEach
  void setUp() {
    shoppingCart = new ShoppingCart();
  }

  @Test
  void shouldAddUniqueProduct() {
    boolean result = shoppingCart.addProducts("tv", 100, 1);

    assertTrue(result);
  }

  @Test
  void shouldIncreaseProductQuantityIfAddsTheSameAgain() {
    boolean result1 = shoppingCart.addProducts("tv", 100, 1);
    boolean result2 = shoppingCart.addProducts("tv", 100, 1);

    assertTrue(result1);
    assertTrue(result2);
  }

  @Test
  void shouldDoNotMultipleProductsWithTheSameNameButDifferentPrice() {
    boolean result1 = shoppingCart.addProducts("tv", 100, 1);
    boolean result2 = shoppingCart.addProducts("tv", 90, 1);

    assertTrue(result1);
    assertFalse(result2);
  }

  @Test
  void shouldDoNotAddProductIfPriceIsLessThanZero() {
    boolean result = shoppingCart.addProducts("tv", -10, 1);

    assertFalse(result);
  }

  @Test
  void shouldDoNotAddProductIfPriceIsEqualZero() {
    boolean result = shoppingCart.addProducts("tv", 0, 1);

    assertFalse(result);
  }

  @Test
  void shouldDoNotAddProductIfQuantityIsLessThanZero() {
    boolean result = shoppingCart.addProducts("tv", 100, -1);

    assertFalse(result);
  }

  @Test
  void shouldDoNotAddProductIfQuantityIsEqualZero() {
    boolean result = shoppingCart.addProducts("tv", 30, 0);

    assertFalse(result);
  }

  @Test
  void shouldDoNotAddProductIfCartIsFull() {
    shoppingCart.addProducts("tv", 100, PRODUCTS_LIMIT);

    boolean result = shoppingCart.addProducts("pc", 200, 1);

    assertFalse(result);
  }

  @Test
  void shouldDoNotIncreaseProductAmountIfCartIsFull() {
    shoppingCart.addProducts("tv", 100, PRODUCTS_LIMIT);

    boolean result = shoppingCart.addProducts("tv", 100, 1);

    assertFalse(result);
  }

  @Test
  void shouldDeleteProductIfExists() {
    shoppingCart.addProducts("tv", 100, 1);

    boolean result = shoppingCart.deleteProducts("tv", 1);

    assertTrue(result);
  }

  @Test
  void shouldDeleteProductIfQuantityIsLessThanQuantityInCart() {
    shoppingCart.addProducts("tv", 100, 3);

    boolean result = shoppingCart.deleteProducts("tv", 2);

    assertTrue(result);
  }

  @Test
  void shouldDeleteSomeOfProductQuantityIfInCartQuantityIsBigger() {
    shoppingCart.addProducts("tv", 100, 5);

    boolean result = shoppingCart.deleteProducts("tv", 2);

    assertTrue(result);
    assertEquals(3, shoppingCart.getQuantityOfProduct("tv"));
  }

  @Test
  void shouldDeleteProductIfQuantityIsEqualToQuantityInCart() {
    shoppingCart.addProducts("tv", 100, 3);

    boolean result = shoppingCart.deleteProducts("tv", 3);

    assertTrue(result);
  }

  @Test
  void shouldDoNotDeleteProductIfQuantityIsBiggerThanQuantityInCart() {
    shoppingCart.addProducts("tv", 100, 2);

    boolean result = shoppingCart.deleteProducts("tv", 4);

    assertFalse(result);
  }

  @Test
  void shouldDoNotDeleteProductIfDoNotExistsInCart() {
    boolean result = shoppingCart.deleteProducts("tv", 1);

    assertFalse(result);
  }

  @Test
  void shouldReturnQuantityEqualToZeroIfProductIsNotInCart() {
    int quantity = shoppingCart.getQuantityOfProduct("tv");

    assertEquals(0, quantity);
  }

  @Test
  void shouldReturnProductQuantityInCart() {
    shoppingCart.addProducts("tv", 100, 2);
    shoppingCart.addProducts("tv", 100, 3);

    int quantity = shoppingCart.getQuantityOfProduct("tv");

    assertEquals(5, quantity);
  }

  @Test
  void shouldReturnSumOfPricesInCart() {
    shoppingCart.addProducts("tv", 100, 3);
    shoppingCart.addProducts("pc", 300, 2);

    int sumOfPrices = shoppingCart.getSumProductsPrices();

    assertEquals(900, sumOfPrices);
  }

  @Test
  void shouldReturnSumOfPricesEqualToZeroIfCartIsEmpty() {
    int sumOfPrices = shoppingCart.getSumProductsPrices();

    assertEquals(0, sumOfPrices);
  }

  @Test
  void shouldReturnProductPriceIfProductIsInCart() {
    shoppingCart.addProducts("tv", 100, 3);

    int price = shoppingCart.getProductPrice("tv");

    assertEquals(100, price);
  }

  @Test
  void shouldReturnProductPriceEqualToZeroIfProductDoNotExistsInCart() {
    int price = shoppingCart.getProductPrice("tv");

    assertEquals(0, price);
  }

  @Test
  void shouldReturnListOfProductNames() {
    shoppingCart.addProducts("tv", 100, 3);
    shoppingCart.addProducts("pc", 300, 2);

    List<String> productNames = shoppingCart.getProductsNames();

    assertEquals(2, productNames.size());
    assertEquals("tv", productNames.get(0));
    assertEquals("pc", productNames.get(1));
  }

  @Test
  void shouldReturnEmptyListOfProductNamesIfCartIsEmpty() {
    List<String> productNames = shoppingCart.getProductsNames();

    assertEquals(0, productNames.size());
  }
}
