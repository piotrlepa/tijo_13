package pl.edu.pwsztar.repository.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Optional;
import pl.edu.pwsztar.entity.Product;
import pl.edu.pwsztar.repository.ProductRepository;

public class ProductRepositoryImpl implements ProductRepository {

  private HashMap<String, Product> products = new HashMap<>();

  public void upsert(Product product) {
    products.put(product.getName(), product);
  }

  public boolean deleteByName(String name) {
    Product deletedProduct = products.remove(name);
    return deletedProduct != null;
  }

  public Optional<Product> getProductByName(String name) {
    return Optional.ofNullable(products.get(name));
  }

  public Collection<Product> getAllProducts() {
    return products.values();
  }
}
